package com.craftincode.ekj.advanced.springlibrary.springlibrary.controller.bookrentals.dto;

public class CreateBookRentalDto {
    private String login;
    private Integer bookId;

    public CreateBookRentalDto() {

    }

    public CreateBookRentalDto(String login, Integer bookId) {

        this.login = login;
        this.bookId = bookId;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public Integer getBookId() {
        return bookId;
    }

    public void setBookId(Integer bookId) {
        this.bookId = bookId;
    }
}
