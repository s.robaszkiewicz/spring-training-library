package com.craftincode.ekj.advanced.springlibrary.springlibrary.controller.users;

import com.craftincode.ekj.advanced.springlibrary.springlibrary.controller.UnauthorizedException;
import com.craftincode.ekj.advanced.springlibrary.springlibrary.controller.users.dto.CreateUserDto;
import com.craftincode.ekj.advanced.springlibrary.springlibrary.controller.users.dto.UpdateUserDto;
import com.craftincode.ekj.advanced.springlibrary.springlibrary.controller.users.dto.UserDto;
import com.craftincode.ekj.advanced.springlibrary.springlibrary.service.UserService;
import com.craftincode.ekj.advanced.springlibrary.springlibrary.service.exceptions.UserAlreadyExists;
import com.craftincode.ekj.advanced.springlibrary.springlibrary.service.exceptions.UserNotFound;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import java.security.Principal;
import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/api/v1/users")
public class UserController {

    @Autowired
    private UserService userService;

    @RolesAllowed({"ADMIN"})
    @RequestMapping(method = RequestMethod.GET)
    public List<UserDto> getAllUsers() {
        return userService.getAllUsers();
    }

    @RolesAllowed({"USER", "ADMIN", "XYZ"})
    @RequestMapping(method = RequestMethod.GET, value = "/{login}")
    public UserDto getByLogin(@PathVariable String login, Authentication authentication) throws UserNotFound, UnauthorizedException {
        String username = authentication.getName();

        if(!login.equals(username)){
            throw new UnauthorizedException("You cannot view other users data");
        }
        return userService.getByLogin(login);
    }

    @RolesAllowed({"USER", "ADMIN"})
    @RequestMapping(method = RequestMethod.POST)
    public UserDto createUser(@RequestBody CreateUserDto createUserDto) throws UserAlreadyExists {
        return userService.createUser(createUserDto);
    }

    @RolesAllowed({"USER", "ADMIN"})
    @RequestMapping(method = RequestMethod.PUT, value = "/{login}")
    public UserDto updateUser(@PathVariable String login, @RequestBody UpdateUserDto updateUserDto) throws UserNotFound {
        // TODO dopisać autoryzację - sprawdzenie, czy użytkownik aktualizuje swoje dane
        return userService.updateUser(login, updateUserDto);
    }

    @RolesAllowed({"USER", "ADMIN"})
    @RequestMapping(method = RequestMethod.DELETE, value = "/{login}")
    public void deleteUser(@PathVariable String login) throws UserNotFound {
        // TODO dopisać autoryzację - sprawdzenie, czy użytkownik usuwa swoje dane
        userService.deleteUser(login);
    }


}
