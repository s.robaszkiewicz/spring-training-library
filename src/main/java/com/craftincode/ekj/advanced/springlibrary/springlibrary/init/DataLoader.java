package com.craftincode.ekj.advanced.springlibrary.springlibrary.init;

import com.craftincode.ekj.advanced.springlibrary.springlibrary.model.Book;
import com.craftincode.ekj.advanced.springlibrary.springlibrary.repository.BooksRepository;
import com.craftincode.ekj.advanced.springlibrary.springlibrary.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class DataLoader {

    @Autowired
    private BooksRepository bookRepository;
    @Autowired
    private UsersRepository usersRepository;

    @PostConstruct
    public void init() {
//        bookRepository.save(new Book("Rok 1984", "George Orwell", 1949, "fiction"));
    }
}
