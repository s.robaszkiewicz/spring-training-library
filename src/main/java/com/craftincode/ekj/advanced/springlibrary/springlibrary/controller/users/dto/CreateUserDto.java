package com.craftincode.ekj.advanced.springlibrary.springlibrary.controller.users.dto;

public class CreateUserDto extends UserDto {
    private String password;

    public CreateUserDto() {
    }

    public CreateUserDto(String login, String city, String userRoles, int birthYear, String password) {
        super(login, city, userRoles, birthYear);
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
