package com.craftincode.ekj.advanced.springlibrary.springlibrary.security;

import com.craftincode.ekj.advanced.springlibrary.springlibrary.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class LibraryUserDetailsService implements UserDetailsService {
    @Autowired
    private UsersRepository usersRepository;

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        com.craftincode.ekj.advanced.springlibrary.springlibrary.model.User userModel = usersRepository.findOne(login);
        if (userModel == null) {
            throw new UsernameNotFoundException("User with login " + login + " does not exists");
        } else {
            String userRolesString = userModel.getUserRoles();
            String[] userRoles = userRolesString.split(",");

            List<GrantedAuthority> grantedAuthorityList = new ArrayList<>();
            for (String userRole : userRoles) {
                grantedAuthorityList.add(new SimpleGrantedAuthority("ROLE_" + userRole));
            }
            return new User(userModel.getLogin(), userModel.getPassword(), grantedAuthorityList);
        }
    }
}
