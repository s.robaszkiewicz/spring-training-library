package com.craftincode.ekj.advanced.springlibrary.springlibrary.repository;

import com.craftincode.ekj.advanced.springlibrary.springlibrary.model.Book;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BooksRepository extends CrudRepository<Book, Integer> {
}
