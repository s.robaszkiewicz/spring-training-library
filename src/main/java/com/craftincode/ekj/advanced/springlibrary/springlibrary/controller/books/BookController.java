package com.craftincode.ekj.advanced.springlibrary.springlibrary.controller.books;

import com.craftincode.ekj.advanced.springlibrary.springlibrary.controller.books.dto.BookDto;
import com.craftincode.ekj.advanced.springlibrary.springlibrary.controller.books.dto.CreateUpdateBookDto;
import com.craftincode.ekj.advanced.springlibrary.springlibrary.service.BookService;
import com.craftincode.ekj.advanced.springlibrary.springlibrary.service.exceptions.BookNotFound;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import java.util.List;

@RestController
@RequestMapping("/api/v1/books")
public class BookController {

    @Autowired
    private BookService bookService;

    @RolesAllowed({"USER","ADMIN"})
    @RequestMapping(method = RequestMethod.GET)
    public List<BookDto> getAllBooks() {
        return bookService.getAllBooks();
    }

    @RolesAllowed({"USER","ADMIN"})
    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public BookDto getById(@PathVariable Integer id) throws BookNotFound {
        // SPOSÓB NA ZWRACANIE KODÓW ODPOWIEDZI Z UŻYCIEM RESPONSE ENTITY
//        try {
//            Book book = bookService.getBookById(id);
//            BookDto bookDto = transformModelToDto(book);
//            return ResponseEntity.ok(bookDto);
//        } catch (BookNotFound bookNotFound) {
//            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//        }

        return bookService.getBookById(id);
    }

    @RolesAllowed({"ADMIN"})
    @RequestMapping(method = RequestMethod.POST)
    public BookDto createBook(@RequestBody CreateUpdateBookDto createUpdateBookDto) {
        return bookService.createBook(createUpdateBookDto);
    }

    @RolesAllowed({"ADMIN"})
    @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
    public BookDto updateBook(@PathVariable Integer id, @RequestBody CreateUpdateBookDto createUpdateBookDto) throws BookNotFound {
        return bookService.updateBook(id, createUpdateBookDto);
    }

    @RolesAllowed({"ADMIN"})
    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public void deleteBook(@PathVariable Integer id) throws BookNotFound {
        bookService.deleteBook(id);
    }

    @RolesAllowed({"USER", "ADMIN"})
    @RequestMapping(method = RequestMethod.GET, value = "/available")
    public List<BookDto> deleteBook() {
        // TODO - dopisać endpoint zwracający wyłącznie dostępne książki
        return null;
    }
}
