package com.craftincode.ekj.advanced.springlibrary.springlibrary.service.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class BookAlreadyRented extends Exception {
    public BookAlreadyRented(String message) {
        super(message);
    }
}
