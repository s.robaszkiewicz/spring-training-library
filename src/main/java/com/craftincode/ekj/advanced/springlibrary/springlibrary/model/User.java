package com.craftincode.ekj.advanced.springlibrary.springlibrary.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class User {
    @Id
    private String login;
    private String password;
    private String city;
    private int birthYear;

    // Role zapisane w Stringu, oddzielone przecinkami, np. "ADMIN,USER"
    private String userRoles;
    // TODO - zmienić sposób przechowywania ról ze Stringa na osobą tabelę user_roles(user_id, role)

    public String getUserRoles() {
        return userRoles;
    }

    public void setUserRoles(String userRoles) {
        this.userRoles = userRoles;
    }

    public User() {
    }

    public User(String login, String password, String city, int birthYear, String userRoles) {
        this.login = login;
        this.password = password;
        this.city = city;
        this.birthYear = birthYear;
        this.userRoles = userRoles;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getBirthYear() {
        return birthYear;
    }

    public void setBirthYear(int birthYear) {
        this.birthYear = birthYear;
    }
}
