package com.craftincode.ekj.advanced.springlibrary.springlibrary.repository;

import com.craftincode.ekj.advanced.springlibrary.springlibrary.model.Book;
import com.craftincode.ekj.advanced.springlibrary.springlibrary.model.BookRental;
import com.craftincode.ekj.advanced.springlibrary.springlibrary.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookRentalsRepository extends CrudRepository<BookRental, Integer> {
    boolean findByBookAndToDateIsNull(Book book);
}
