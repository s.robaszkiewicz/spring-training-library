package com.craftincode.ekj.advanced.springlibrary.springlibrary.service;

import com.craftincode.ekj.advanced.springlibrary.springlibrary.controller.bookrentals.dto.BookRentalDto;
import com.craftincode.ekj.advanced.springlibrary.springlibrary.controller.bookrentals.dto.CreateBookRentalDto;
import com.craftincode.ekj.advanced.springlibrary.springlibrary.model.Book;
import com.craftincode.ekj.advanced.springlibrary.springlibrary.model.BookRental;
import com.craftincode.ekj.advanced.springlibrary.springlibrary.model.User;
import com.craftincode.ekj.advanced.springlibrary.springlibrary.repository.BookRentalsRepository;
import com.craftincode.ekj.advanced.springlibrary.springlibrary.repository.BooksRepository;
import com.craftincode.ekj.advanced.springlibrary.springlibrary.repository.UsersRepository;
import com.craftincode.ekj.advanced.springlibrary.springlibrary.service.exceptions.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class BookRentalsService {

    @Autowired
    private BookRentalsRepository rentalsRepository;
    @Autowired
    private UsersRepository usersRepository;
    @Autowired
    private BooksRepository booksRepository;

    public List<BookRentalDto> getAllRentals() {
        Iterable<BookRental> rentalIterable = rentalsRepository.findAll();
        List<BookRentalDto> bookRentalDtos = new ArrayList<>();

        for (BookRental bookRental : rentalIterable) {
            bookRentalDtos.add(transformModelToDto(bookRental));
        }

        return bookRentalDtos;
    }

    private BookRentalDto transformModelToDto(BookRental bookRental) {
        BookRentalDto bookRentalDto = new BookRentalDto();
        bookRentalDto.setRentalId(bookRental.getId());
        bookRentalDto.setBookId(bookRental.getBook().getId());
        bookRentalDto.setLogin(bookRental.getUser().getLogin());
        bookRentalDto.setFromDate(bookRental.getFromDate());
        bookRentalDto.setToDate(bookRental.getToDate());

        return bookRentalDto;
    }

    public BookRentalDto createRental(CreateBookRentalDto createBookRentalDto) throws UserNotFound, BookNotFound, BookAlreadyRented {

        User user = usersRepository.findOne(createBookRentalDto.getLogin());

        if (user == null) {
            throw new UserNotFound("User with login " + createBookRentalDto.getLogin() + " does not exists");
        }

        Book book = booksRepository.findOne(createBookRentalDto.getBookId());

        if (book == null) {
            throw new BookNotFound("Book with id " + createBookRentalDto.getBookId() + " does not exists");
        }

        boolean isBookRented = book.getRentals().stream().anyMatch(x -> x.getToDate() == null);

        if (isBookRented) {
            throw new BookAlreadyRented("Book with id " + book.getId() + " is already rented");
        }

        BookRental bookRental = new BookRental(user, book, new Date(), null);
        BookRental savedBookRental = rentalsRepository.save(bookRental);
        return transformModelToDto(savedBookRental);

    }

    public BookRentalDto finishRental(Integer rentalId) throws RentalNotFound, RentalAlreadyFinished {
        BookRental rental = rentalsRepository.findOne(rentalId);
        if (rental == null) {
            throw new RentalNotFound("Rental with id " + rentalId + " does not exists");
        }
        if (rental.getToDate() != null) {
            throw new RentalAlreadyFinished("Rental with id " + rentalId + " is already finished");
        }

        rental.setToDate(new Date());
        BookRental savedRental = rentalsRepository.save(rental);
        return transformModelToDto(savedRental);
    }

    public BookRentalDto getRentalById(Integer rentalId) throws RentalNotFound {
        BookRental rental = rentalsRepository.findOne(rentalId);
        if (rental == null) {
            throw new RentalNotFound("Rental with id " + rentalId + " does not exists");
        }
        return transformModelToDto(rental);
    }
}
