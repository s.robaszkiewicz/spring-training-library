package com.craftincode.ekj.advanced.springlibrary.springlibrary.controller.users.dto;

public class UserDto {
    private String login;
    private String city;
    private String userRoles;
    private int birthYear;

    public UserDto() {
    }

    public UserDto(String login, String city, String userRoles, int birthYear) {
        this.login = login;
        this.city = city;
        this.userRoles = userRoles;
        this.birthYear = birthYear;
    }

    public String getUserRoles() {

        return userRoles;
    }

    public void setUserRoles(String userRoles) {
        this.userRoles = userRoles;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getBirthYear() {
        return birthYear;
    }

    public void setBirthYear(int birthYear) {
        this.birthYear = birthYear;
    }
}
