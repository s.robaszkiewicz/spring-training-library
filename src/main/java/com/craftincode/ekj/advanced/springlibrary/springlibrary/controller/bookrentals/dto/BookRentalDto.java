package com.craftincode.ekj.advanced.springlibrary.springlibrary.controller.bookrentals.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class BookRentalDto {
    private Integer rentalId;
    private String login;
    private Integer bookId;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm")
    private Date fromDate;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm")
    private Date toDate;

    public BookRentalDto() {

    }

    public BookRentalDto(Integer rentalId, String login, Integer bookId, Date fromDate, Date toDate) {

        this.rentalId = rentalId;
        this.login = login;
        this.bookId = bookId;
        this.fromDate = fromDate;
        this.toDate = toDate;
    }

    public Integer getRentalId() {
        return rentalId;
    }

    public void setRentalId(Integer rentalId) {
        this.rentalId = rentalId;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public Integer getBookId() {
        return bookId;
    }

    public void setBookId(Integer bookId) {
        this.bookId = bookId;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }
}
