package com.craftincode.ekj.advanced.springlibrary.springlibrary.model;


import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String name;
    private String author;
    private Integer year;
    private String genre;
    @OneToMany(mappedBy = "book")
    private List<BookRental> rentals = new ArrayList<>();

    public Book() {
    }

    public Book(String name, String author, int year, String genre) {
        this.name = name;
        this.author = author;
        this.year = year;
        this.genre = genre;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public List<BookRental> getRentals() {
        return rentals;
    }

    public void setRentals(List<BookRental> rentals) {
        this.rentals = rentals;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }
}
