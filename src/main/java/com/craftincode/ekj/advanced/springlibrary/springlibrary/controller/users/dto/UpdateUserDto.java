package com.craftincode.ekj.advanced.springlibrary.springlibrary.controller.users.dto;

public class UpdateUserDto {
    private String city;
    private int birthYear;
    private String password;

    public UpdateUserDto(String city, int birthYear, String password) {
        this.city = city;
        this.birthYear = birthYear;
        this.password = password;
    }

    public UpdateUserDto() {
    }


    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getBirthYear() {
        return birthYear;
    }

    public void setBirthYear(int birthYear) {
        this.birthYear = birthYear;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
