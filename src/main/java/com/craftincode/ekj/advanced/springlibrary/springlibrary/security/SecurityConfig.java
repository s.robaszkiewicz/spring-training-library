package com.craftincode.ekj.advanced.springlibrary.springlibrary.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true, jsr250Enabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private LibraryUserDetailsService libraryUserDetailsService;

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http
                .csrf().disable()
                .authorizeRequests()

                // ustawienie różnych poziomów zabezpieczeń dla podstron
//                .antMatchers("/**").permitAll()
//                .antMatchers(HttpMethod.GET, "/admin/**").hasRole("ADMIN")
//                .antMatchers(HttpMethod.POST, "/books").hasRole("ADMIN")
//                .antMatchers(HttpMethod.PUT, "/employees/**").hasRole("ADMIN")
//                .antMatchers(HttpMethod.PATCH, "/employees/**").hasRole("ADMIN")
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .permitAll()

                // ustawienie własnego formularza logowania
//                .and()
//                .formLogin().loginPage("/login").passwordParameter("password").usernameParameter("username")
//                .successForwardUrl("/").failureForwardUrl("")
//
                .and()
                .logout()
                .permitAll()
                .and()
                .exceptionHandling().accessDeniedPage("/unauthorized.html");


    }

    @Autowired
    public void configureGlobalSecurity(AuthenticationManagerBuilder auth) throws Exception {
        // IN-MEMORY - stworzenie listy użytkowników przy wstawaniu aplikacji
//        auth
//                .inMemoryAuthentication()
//                .withUser("user").password("user").roles("USER")
//                .and()
//                .withUser("admin").password("admin").roles("ADMIN", "USER");

        // USER-DETAILS-SERVICE - użycie serwisu zwracającego dane o użytkownikach
        // W tym wypadku LibraryUserDetailsService, który korzysta z UsersRepository
        auth.userDetailsService(libraryUserDetailsService);


        // TODO - dla chętnych - skonfigurować BCryptPasswordEncoder, przy zapisywaniu użytkownika hasło powinno
        // zostać "zahashowane" z użycie BCrypt (można stworzyć beana z BCryptPasswordEncoder i użyć go w UserService)
        // UŻYCIE DAO Authentication Provider z wpięciem "szyfratora" haseł
//        DaoAuthenticationProvider daoAuthenticationProvider = new DaoAuthenticationProvider();
//        daoAuthenticationProvider.setUserDetailsService(libraryUserDetailsService);
//        daoAuthenticationProvider.setPasswordEncoder(new BCryptPasswordEncoder());
//        auth.authenticationProvider(daoAuthenticationProvider);
    }

}
