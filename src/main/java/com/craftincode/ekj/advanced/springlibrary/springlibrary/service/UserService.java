package com.craftincode.ekj.advanced.springlibrary.springlibrary.service;

import com.craftincode.ekj.advanced.springlibrary.springlibrary.controller.users.dto.CreateUserDto;
import com.craftincode.ekj.advanced.springlibrary.springlibrary.controller.users.dto.UpdateUserDto;
import com.craftincode.ekj.advanced.springlibrary.springlibrary.controller.users.dto.UserDto;
import com.craftincode.ekj.advanced.springlibrary.springlibrary.model.User;
import com.craftincode.ekj.advanced.springlibrary.springlibrary.repository.UsersRepository;
import com.craftincode.ekj.advanced.springlibrary.springlibrary.service.exceptions.UserAlreadyExists;
import com.craftincode.ekj.advanced.springlibrary.springlibrary.service.exceptions.UserNotFound;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserService {

    @Autowired
    private UsersRepository usersRepository;


    public List<UserDto> getAllUsers() {
        Iterable<User> userIterable = usersRepository.findAll();
        List<UserDto> userDtos = new ArrayList<>();

        for (User userModel : userIterable) {
            userDtos.add(transformModelToDto(userModel));
        }

        return userDtos;
    }


    public UserDto getByLogin(String login) throws UserNotFound {
        User user = getUserModelByLogin(login);
        return transformModelToDto(user);
    }


    public UserDto createUser(CreateUserDto createUserDto) throws UserAlreadyExists {
        User existingUser = usersRepository.findOne(createUserDto.getLogin());

        if (existingUser != null) {
            throw new UserAlreadyExists("User with login " + createUserDto.getLogin() + " already exists!");
        }

        User userToSave = transformCreateDtoToModel(createUserDto);
        User savedUser = usersRepository.save(userToSave);
        return transformModelToDto(savedUser);
    }

    public UserDto updateUser(String login, UpdateUserDto updateUserDto) throws UserNotFound {
        User user = getUserModelByLogin(login);

        user.setCity(updateUserDto.getCity());
        user.setBirthYear(updateUserDto.getBirthYear());
        user.setPassword(updateUserDto.getPassword());

        User updatedUser = usersRepository.save(user);
        return transformModelToDto(updatedUser);
    }

    public void deleteUser(String login) throws UserNotFound {
        User user = getUserModelByLogin(login);

        usersRepository.delete(user);
    }

    private User getUserModelByLogin(String login) throws UserNotFound {
        User user = usersRepository.findOne(login);
        if (user == null) {
            throw new UserNotFound("User with login " + login + " does not exists");
        }
        return user;
    }

    private UserDto transformModelToDto(User userModel) {
        return new UserDto(userModel.getLogin(), userModel.getCity(), userModel.getUserRoles(), userModel.getBirthYear());
    }

    private User transformCreateDtoToModel(CreateUserDto createUserDto) {
        return new User(createUserDto.getLogin(), createUserDto.getPassword(),
                createUserDto.getCity(), createUserDto.getBirthYear(), createUserDto.getUserRoles());
    }
}
