package com.craftincode.ekj.advanced.springlibrary.springlibrary.controller.books.dto;

public class CreateUpdateBookDto {
    protected String name;
    protected String author;
    protected int year;
    protected String genre;

    public CreateUpdateBookDto(String name, String author, int year, String genre) {
        this.name = name;
        this.author = author;
        this.year = year;
        this.genre = genre;
    }

    public CreateUpdateBookDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }
}
