package com.craftincode.ekj.advanced.springlibrary.springlibrary.controller.bookrentals;

import com.craftincode.ekj.advanced.springlibrary.springlibrary.controller.UnauthorizedException;
import com.craftincode.ekj.advanced.springlibrary.springlibrary.controller.bookrentals.dto.BookRentalDto;
import com.craftincode.ekj.advanced.springlibrary.springlibrary.controller.bookrentals.dto.CreateBookRentalDto;
import com.craftincode.ekj.advanced.springlibrary.springlibrary.service.BookRentalsService;
import com.craftincode.ekj.advanced.springlibrary.springlibrary.service.exceptions.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import java.util.List;

@RestController
@RequestMapping("/api/v1/rentals")
public class BookRentalsController {

    @Autowired
    private BookRentalsService bookRentalsService;

    @RolesAllowed({"ADMIN"})
    @RequestMapping(method = RequestMethod.GET)
    public List<BookRentalDto> getAllRentals() {
        return bookRentalsService.getAllRentals();
    }


    @RolesAllowed({"USER", "ADMIN"})
    @RequestMapping(method = RequestMethod.GET, value = "/{rentalId}")
    public BookRentalDto getRentalById(@PathVariable Integer rentalId) throws RentalNotFound {
        // TODO dodać autoryzację - sprawdzenie, czy użytkownik pobiera dane swojego wypożyczenia
        return bookRentalsService.getRentalById(rentalId);
    }

    @RolesAllowed({"USER", "ADMIN"})
    @RequestMapping(method = RequestMethod.POST)
    public BookRentalDto createRental(@RequestBody CreateBookRentalDto createBookRentalDto) throws BookNotFound, BookAlreadyRented, UserNotFound {
        // TODO dodać autoryzację - sprawdzenie, czy użytkownik tworzy wypożyczenie dla siebie
        return bookRentalsService.createRental(createBookRentalDto);
    }

    @RolesAllowed({"USER", "ADMIN"})
    @RequestMapping(method = RequestMethod.POST, value = "/return/{rentalId}")
    public BookRentalDto finishRental(@PathVariable Integer rentalId, Authentication authentication) throws RentalNotFound, UnauthorizedException, RentalAlreadyFinished {
        BookRentalDto rental = bookRentalsService.getRentalById(rentalId);

        if (rental.getLogin().equals(authentication.getName())) {
            return bookRentalsService.finishRental(rentalId);
        } else {
            throw new UnauthorizedException("Viewing other users rentals in not allowed");
        }
    }


}
