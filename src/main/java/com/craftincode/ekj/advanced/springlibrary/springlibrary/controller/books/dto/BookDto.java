package com.craftincode.ekj.advanced.springlibrary.springlibrary.controller.books.dto;

public class BookDto extends CreateUpdateBookDto {
    private int id;

    public BookDto(int id, String name, String author, int year, String genre) {
        super(name, author, year, genre);
        this.id = id;
    }

    public BookDto() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

}
