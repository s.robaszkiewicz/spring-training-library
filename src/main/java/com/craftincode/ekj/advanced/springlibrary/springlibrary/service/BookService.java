package com.craftincode.ekj.advanced.springlibrary.springlibrary.service;

import com.craftincode.ekj.advanced.springlibrary.springlibrary.controller.books.dto.BookDto;
import com.craftincode.ekj.advanced.springlibrary.springlibrary.controller.books.dto.CreateUpdateBookDto;
import com.craftincode.ekj.advanced.springlibrary.springlibrary.model.Book;
import com.craftincode.ekj.advanced.springlibrary.springlibrary.repository.BooksRepository;
import com.craftincode.ekj.advanced.springlibrary.springlibrary.service.exceptions.BookNotFound;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class BookService {

    @Autowired
    private BooksRepository bookRepository;

    public List<BookDto> getAllBooks() {
        Iterable<Book> bookIterable = bookRepository.findAll();

        List<BookDto> booksList = new ArrayList<>();
        for (Book book : bookIterable) {
            booksList.add(transformModelToDto(book));
        }

        return booksList;
    }

    public BookDto getBookById(Integer id) throws BookNotFound {
        Book book = bookRepository.findOne(id);
        if (book == null) {
            throw new BookNotFound("Book with id " + id + " not found");
        }

        return transformModelToDto(book);
    }

    public BookDto createBook(CreateUpdateBookDto createUpdateBookDto) {
        Book bookToSave = transformCrateBookDtoToModel(createUpdateBookDto);
        Book savedBook = bookRepository.save(bookToSave);
        return transformModelToDto(savedBook);
    }


    public BookDto updateBook(Integer id, CreateUpdateBookDto createUpdateBookDto) throws BookNotFound {
        Book existingBook = bookRepository.findOne(id);
        if (existingBook == null) {
            throw new BookNotFound("Book with id " + id + " not found");
        }

        existingBook.setAuthor(createUpdateBookDto.getAuthor());
        existingBook.setGenre(createUpdateBookDto.getGenre());
        existingBook.setYear(createUpdateBookDto.getYear());
        existingBook.setName(createUpdateBookDto.getName());

        Book savedBook = bookRepository.save(existingBook);
        return transformModelToDto(savedBook);
    }

    private BookDto transformModelToDto(Book book) {
        return new BookDto(book.getId(), book.getName(), book.getAuthor(), book.getYear(), book.getGenre());
    }


    private Book transformCrateBookDtoToModel(CreateUpdateBookDto createUpdateBookDto) {
        return new Book(createUpdateBookDto.getName(), createUpdateBookDto.getAuthor(),
                createUpdateBookDto.getYear(), createUpdateBookDto.getGenre());
    }

    public void deleteBook(Integer id) throws BookNotFound {
        Book existingBook = bookRepository.findOne(id);
        if (existingBook == null) {
            throw new BookNotFound("Book with id " + id + " not found");
        }

        bookRepository.delete(existingBook);
    }
}
