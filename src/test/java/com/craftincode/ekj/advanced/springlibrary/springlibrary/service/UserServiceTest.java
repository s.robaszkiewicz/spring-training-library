package com.craftincode.ekj.advanced.springlibrary.springlibrary.service;

import com.craftincode.ekj.advanced.springlibrary.springlibrary.controller.users.dto.CreateUserDto;
import com.craftincode.ekj.advanced.springlibrary.springlibrary.model.User;
import com.craftincode.ekj.advanced.springlibrary.springlibrary.repository.UsersRepository;
import com.craftincode.ekj.advanced.springlibrary.springlibrary.service.exceptions.UserAlreadyExists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

    @Mock
    UsersRepository usersRepository;

    @InjectMocks
    UserService userService = new UserService();

    @Test(expected = UserAlreadyExists.class)
    public void createUser_shouldThrowExceptionForExistingUser() throws Exception {
        when(usersRepository.findOne("admin")).thenReturn(new User());

        CreateUserDto createUserDto = new CreateUserDto("admin", "Wrocław", "ADMIN", 1980, "admin");

        userService.createUser(createUserDto);
    }

    @Test(expected = UserAlreadyExists.class)
    public void createUser_shouldReturnDto() throws Exception {
        when(usersRepository.findOne("admin")).thenReturn(new User());

        CreateUserDto createUserDto = new CreateUserDto("admin", "Wrocław", "ADMIN", 1980, "admin");

        userService.createUser(createUserDto);
    }

}
