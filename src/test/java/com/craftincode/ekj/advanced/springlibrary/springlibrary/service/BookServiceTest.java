package com.craftincode.ekj.advanced.springlibrary.springlibrary.service;

import com.craftincode.ekj.advanced.springlibrary.springlibrary.controller.books.dto.BookDto;
import com.craftincode.ekj.advanced.springlibrary.springlibrary.model.Book;
import com.craftincode.ekj.advanced.springlibrary.springlibrary.repository.BooksRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class BookServiceTest {

    @Mock
    private BooksRepository booksRepository;

    @InjectMocks
    private BookService bookService = new BookService();

    @Before
    public void setup() {
    }

    @Test
    public void getAllBooks() throws Exception {
        when(booksRepository.findAll()).thenReturn(new ArrayList<>());
        List<BookDto> allBooks = bookService.getAllBooks();

        assertThat(allBooks.size()).isEqualTo(0);
//        assertEquals(allBooks);

    }

    @Test
    public void getBookById() throws Exception {
        when(booksRepository.findOne(anyInt())).thenReturn(new Book("name", "xyz", 100, "b"));

        bookService.getBookById(1);

//        verify(booksRepository, times(1)).findOne(1);
//        assertEquals(allBooks);

    }
}
