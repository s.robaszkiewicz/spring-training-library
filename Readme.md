### Generator aplikacji Spring
[https://start.spring.io/](https://start.spring.io/)

Przy tworzeniu aplikacji dodaj zależność `WEB` i `JPA`

### Stworzenie bazy danych w MySQL
```
mysqladmin -u root -p create spring-library
```

### Dodanie connectora do MySQL
W `pom.xml`

```xml
<dependency>
    <groupId>mysql</groupId>
    <artifactId>mysql-connector-java</artifactId>
</dependency>
```

### Skonfigurowanie połączenia do bazy danych dla JPA
W pliku `application.properties`:

```xml
spring.jpa.hibernate.ddl-auto=update
spring.datasource.url=jdbc:mysql://localhost:3306/spring-library?useLegacyDatetimeCode=false&serverTimezone=UTC
spring.datasource.username=root
spring.datasource.password=mysql_pass
```

## Aplikacja gotowa do uruchomienia!

## SWAGGER
Swagger jest dostępny pod adresem [http://localhost:8080/swagger-ui.html](http://localhost:8080/swagger-ui.html)


### Tworzenie modeli, repozytoriów, serwisów, kontrolerów
Każda encja musi mieć adnotację `@Entity` oraz wyznaczony klucz główny
czyli pole z adnotacją `@Id`


#### Książka
- Model - klasa `Book.java`
- Repozytorium - interfejs `BookRepository.java`
- Kontroler - klasa `BookController.java`


## Dodanie Spring Security
Do `pom.xml` dodać zależność
```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-security</artifactId>
</dependency>
```

Konfiguracja security - `SecurityConfig.java`


## Spring DEV-TOOLS

`Spring DEV-TOOLS` umożliwia automatyczne restartowanie aplikacji gdy tylko zmieni się jakikolwiek kod. 

Włączenie Spring Dev-Tools

1. Dodanie zależności

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-devtools</artifactId>
</dependency>
```

2. W ustawieniach IntelliJ w sekcji `Build, Execution, Deployment` -> `Compiler` włączyć opcję `Build project automatically`

3. W rejestrze (CTRL + SHIFT + A i wyszukać `Registry...`) zaznaczyć opcję 	`compiler.automake.allow.when.app.running`
